package b;

public class Customer implements Runnable {

  private int number;

  public Customer(int number) {
    this.number = number;
  }

  public int getNumber() {
    return number;
  }

  @Override
  public void run() {
    while (true) {
      if (Haircut.sleep.get()) {
        System.out.println("he sleep");
        Main.thread.interrupt();
        Main.thread = new Thread(Main.haircut);
        Main.thread.start();
        return;
      }
    }
  }
}

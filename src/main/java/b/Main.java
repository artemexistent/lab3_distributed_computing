package b;

import lombok.SneakyThrows;

import java.util.concurrent.ArrayBlockingQueue;

public class Main {
  public static ArrayBlockingQueue<Customer> queue = new ArrayBlockingQueue(10000);
  public static Haircut haircut = new Haircut();
  public static Thread thread = new Thread();
  private static int i;

  @SneakyThrows
  public static void main(String[] args) {
    thread = new Thread(haircut);
    thread.start();

    Customer first = new Customer(i++) ;
    queue.add(first);
    new Thread(first).start();

    Customer second = new Customer(i++) ;
    queue.add(second);
    new Thread(second).start();

    Customer third = new Customer(i++) ;
    queue.add(third);
    new Thread(third).start();

    Thread.sleep(30000);
    Customer forth = new Customer(i++);
    queue.add(forth);
    new Thread(forth).start();

  }
}

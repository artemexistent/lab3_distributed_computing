package b;

import lombok.SneakyThrows;

import java.util.concurrent.atomic.AtomicBoolean;

public class Haircut implements Runnable {

  public static AtomicBoolean sleep = new AtomicBoolean(false);

  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      System.out.println("i get up");
      sleep.set(false);
      while (Main.queue.size() != 0) {
        Customer poll = Main.queue.poll();
        System.out.println(poll.getNumber());
        Thread.sleep(1000);
      }
      System.out.println("i sleep");
      sleep.set(true);
      Thread.sleep(10000000);
    }
  }
}

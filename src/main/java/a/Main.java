package a;

public class Main {
  public static final int countBee = 3;
  public static final int potSize = 10;
  public static final Bear bear = new Bear();
  public static Thread threadBear = new Thread();
  public static Integer pot = 0;

  public static void main(String[] args) {
    for (int i = 0; i < countBee; i ++) {
      new Thread(new Bee()).start();
    }
  }

}

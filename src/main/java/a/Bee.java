package a;

import lombok.SneakyThrows;

public class Bee implements Runnable {

  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      synchronized (Main.threadBear) {
        synchronized (Main.pot) {
          if (++Main.pot == Main.potSize) {
            System.out.println(Thread.currentThread() + " beer");
            Main.threadBear = new Thread(Main.bear);
            Main.threadBear.start();
          }
          System.out.println(Thread.currentThread() + " " + Main.pot);
          Thread.sleep(1000);
        }
      }
    }
  }
}

package a;

public class Bear implements Runnable {
  @Override
  public void run() {
    synchronized (Main.pot) {
      Main.pot = 0;
    }
  }
}

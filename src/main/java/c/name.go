package main

import (
	"fmt"
	"math/rand"
	"time"
)

var firstSim = true
var secondSim = true
var thirdSim = true

func firstF() {
	fmt.Println("first")
	firstSim = false
	time.Sleep(1000 * time.Millisecond)
	firstSim = true
}
func secondF() {
	fmt.Println("second")
	secondSim = false
	time.Sleep(2000 * time.Millisecond)
	secondSim = true
}
func thirdF() {
	fmt.Println("third")
	thirdSim = false
	time.Sleep(3000 * time.Millisecond)
	thirdSim = true
}
func midle(first float64, second float64) {
	if first == second || first == 3 || second > 3 || first > second {
		return
	}
	if first == 1 && second == 2 {
		go thirdF()
	}
	if first == 1 && second == 3 {
		go secondF()
	}
	if first == 2 && second == 3 {
		go firstF()
	}
	time.Sleep(1000 * time.Millisecond)
	for !(firstSim && secondSim && thirdSim) {
		time.Sleep(1000 * time.Millisecond)
	}
	fmt.Println("done")
}

var one int

func main() {
	rand.Seed(time.Now().UnixNano())
	fmt.Println("STAAAAART")
	for true {
		b := rand.Intn(5)
		if b > 3 {
			midle(1, 2)
		} else if b > 2 {
			midle(1, 3)
		} else {
			midle(2, 3)
		}
	}
}
